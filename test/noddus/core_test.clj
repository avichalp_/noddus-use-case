(ns noddus.core-test
  (:require [clojure.test :refer :all]
            [noddus.core :refer :all]))

(deftest edn->byte-str-test
  (testing "Testing edn to protobuf byte-str conversion "
    (is (= "Avichal" (edn->byte-str {:name "Avichal", :id 1})))
    (is (= "Alice" (edn->byte-str {:name "Alice", :id 2})))
    (is (= "Bob" (edn->byte-str {:name "Bob", :id 3})))))
