#!/usr/bin/env python3

import sqlite3
import csv
import sys


DB_PATH = 'noddus.db'
TABLE_NAME = 'Noddus'
SCHEMA_QUERY = (
    'CREATE TABLE %s ( '
    'time timestamp, '
    'name varchar, '
    'age integer '
    ')'
) % TABLE_NAME


def process_csv(csv):
    '''Processes data extracted from CSV file.

    :type csv: List[List[str]]
    :rtype: Tuple[List[str], List[List[str]]]
    '''
    rows = []
    headers = []

    for idx, row in enumerate(csv):
        if idx == 0:
            headers = row
        else:
            # padding by empty string for missing values
            for i in range(len(headers) - len(row)):
                row.append('')
            rows.append(row)

    return headers, rows


def main():
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()

    try:
        csvfile_name = sys.argv[1]
        # Read CSV
        with open(csvfile_name) as csvfile:
            _, rows = process_csv(csv.reader(csvfile, delimiter=','))
    except FileNotFoundError as e:
        print('Incorrect filename %s' % csvfile_name)
        exit()

    # Create schema
    cursor.execute('DROP TABLE IF EXISTS %s' % TABLE_NAME)
    cursor.execute(SCHEMA_QUERY)

    # Insert rows
    cursor.executemany('INSERT INTO %s VALUES (?, ?, ?)' % TABLE_NAME, rows)

    conn.commit()
    print('=== Summary ===')
    print('Rows inserted: ', cursor.rowcount)


if __name__ == '__main__':
    main()
