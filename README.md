# Noddus
Implementation of Noddus use case as defined [here](https://drive.google.com/file/d/1rmC62JtFch6PvXWCbi-awoyvm8SnWJn9/view)

## Requirements
1. Docker
2. Docker Compose
3. Python3

Note: This is developed and tested on Docker version 18.09.1 & Docker Compose version 1.23.2 on MacOS but should work on higher versions.


## Usage

### Python CSV import script
An example CSV file is included in this repo.
Run the following command to launch the script.

```
./noddus.py $(pwd)/noddus.csv
```

It assumes `python3` binary to be on $PATH.
If that is not the case run the script by launching python3 interpretor.

```
/path/to/python3 noddus.py $(pwd)/noddus.csv
```

This script will do a *bulk insert* of all rows read from CSV to sqllite database at `$(pwd)/noddus.db`
Prints _number of records inserted_ on stdout after insertion.


### Clojure Web API
This is small Ring + Compojure app that exposes one HTTP POST endpoint on PORT 9060:

- Create a build
```
docker-compose build
```

- Run the container
```
docker-compose up
```
-  Test using curl
```
curl 'http://0.0.0.0:9060/import' -XPOST \
	--data '{"name": "alice", "id": 1}' \
	-H 'Content-type: Application/Json'
```

Json middleware converts JSON input to EDN.
EDN is then converted to protobuf format by using a pre-compiled protobuf schema at `resources/proto/person.proto`.

App runs inside a container. Docker image is based on the openjdk image.
It installs `prtobuf` compiler from `apt` and `leiningen` from the `leiningen` website.
Protobuf compilation happens in an intermediate docker layer. Java code generated after the compilation is put in `./src/main/java`. This path is ignored in source control.
When the container is run, `lein run` is executed.
This compiles the clojure app and also the auto generated java code then it runs the Clojure app which runs the HTTP server.
This is not a production ready way to run the app but a quick hack for now. **Later we can create and deploy an uberjar.**

This repo contains `data/` directory. This is mounted onto the container.
The Web app creates files with serilized protobuf byte strings inside `data/` directory.
Data directory for now is included in the source control **but this should also be removed later**.
All files are named by the timestamp (in miliseconds) when they are created.
On every rollover (3000 ms) a new file is created.

**Note**: New files are not created if there are no incomming requests. On every reqeust the handlers function checks for timeout and performs required actions.
