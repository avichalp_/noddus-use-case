(ns noddus.core
  (:require [clojure.java.io       :as io]
            [clojure.tools.logging :as log]
            [compojure.core        :refer [POST defroutes]]
            [compojure.route       :refer [not-found]]
            [compojure.handler     :as handler]
            [ring.middleware.json  :as json-middleware]
            [ring.adapter.jetty    :refer [run-jetty]]
            [protobuf.core         :as protobuf])
  ;; import `protoc` generated Java classes
  (:import (noddus.person Noddus$Person)))

;; 3 seconds or 3000 miliseconds as rollover timeout
(defonce timeout 3000)
(defonce file-dir "data/")
(def outfile (atom {}))

(defn timed-out?
  [opened-ts]
  (> (- (System/currentTimeMillis) opened-ts) timeout))

(defn append-only-writer
  "Returns an append only `writer` for the given filename."
  [f]
  (io/writer f :append true))

(defn init-state!
  []
  "Initiate the `writer` state."
  (let [now (System/currentTimeMillis)]
    (reset! outfile
            {:writer (append-only-writer (str file-dir now))
             :opened-at now})))

(defn edn->byte-str
  "Converts given EDN data to serilized protobuf
  using per defined proto schema."
  [edn]
  (-> (protobuf/create Noddus$Person edn)
      (protobuf/->bytes)
      (String.)))

(defn store!
  "Takes in the data from `POST` body and stores
  in protobuf format in the file at `./file-dir/<timestamp>`."
  [body]
  (try
    (let [blob   (edn->byte-str body)
          writer (:writer @outfile)
          now    (System/currentTimeMillis)]
      (doto writer
        (.newLine)
        (.write blob)
        (.flush))
      (when (timed-out? (:opened-at @outfile))
        (log/infof "Rolling over after %s miliseconds" timeout)
        (.close writer)
        (reset! outfile {:writer    (append-only-writer (str file-dir now))
                         :opened-at now}))
      "Success")
    (catch Exception e
      (log/error (.getMessage e))
      (str "Failed: " (.getMessage e)))))

(defroutes app-routes
  (POST      "/import" {body :body} (store! body))
  (not-found "<h1>404</h1>"))

(def app
  (-> (handler/site app-routes)
      (json-middleware/wrap-json-body {:keywords? true})
      (json-middleware/wrap-json-response)))

(defn -main
  "Starts `Jetty` server."
  [& args]
  (init-state!)
  (println "Listening on port: 9060")
  (run-jetty app {:port 9060}))

(comment

  (def alice {:name "alice" :id 1})
  (def bob {:name "bob" :id 2})

  (edn->byte-str alice)
  (edn->byte-str bob)

  (store! alice))
