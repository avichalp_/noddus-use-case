FROM openjdk:8
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN apt-get -y update && apt-get install -y libprotobuf-java protobuf-compiler
RUN protoc \
	-I=/usr/include \
	-I=/usr/local/include \
	-I=resources/proto \
	--java_out=src/main/java \
	resources/proto/person.proto

RUN wget 'https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein' && chmod a+x lein && ./lein deps
CMD ["./lein", "run"]


